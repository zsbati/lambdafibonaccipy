cube = lambda x: pow(x, 3)  # complete the lambda function 

def fibonacci(n):
    # return a list of fibonacci numbers
    if n == 0:
        return []
    if n == 1:
        return [0]
    if n == 2:
        return [0, 1]
    li = [0, 1]
    for i in range(2, n):
        li.append(li[i-2] + li[i-1])
    return li

if __name__ == '__main__':
    n = int(input())
    print(list(map(cube, fibonacci(n))))
